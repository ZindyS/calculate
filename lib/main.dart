import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        // useMaterial3: true,
      ),
      home: const MyHomePage(title: 'Flutter Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Scaffold(
        backgroundColor: Colors.grey,
        body: Container(
          margin: const EdgeInsets.all(2),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                        flex: 3,
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          height: double.infinity,
                          color: Colors.black26,
                          alignment: Alignment.center,
                          child: Text(
                            "НАПИШИ",
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold,
                                fontSize: height/12
                            ),
                          ),
                        )
                    ),
                    Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          child: MaterialButton(onPressed: () {  },
                            height: double.infinity,
                            color: Colors.redAccent,
                            child: Text(
                              "CE",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: height/20
                              ),
                            ),
                          ),
                        )
                    )
                  ],
                ),
              ),
              Expanded(
                  child: Row(
                    children: [
                      Expanded(
                          child: Container(
                            margin: const EdgeInsets.all(2),
                            child: MaterialButton(onPressed: () {  },
                              height: double.infinity,
                              color: Colors.white,
                              child: Text(
                                "1",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: height/20
                                ),
                              ),
                            ),
                          )
                      ),
                      Expanded(
                          child: Container(
                            margin: const EdgeInsets.all(2),
                            child: MaterialButton(onPressed: () {  },
                              height: double.infinity,
                              color: Colors.white,
                              child: Text(
                                "2",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: height/20
                                ),
                              ),
                            ),
                          )
                      ),
                      Expanded(
                          child: Container(
                            margin: const EdgeInsets.all(2),
                            child: MaterialButton(onPressed: () {  },
                              height: double.infinity,
                              color: Colors.white,
                              child: Text(
                                "3",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: height/20
                                ),
                              ),
                            ),
                          )
                      ),
                      Expanded(
                          child: Container(
                            margin: const EdgeInsets.all(2),
                            child: MaterialButton(onPressed: () {  },
                              height: double.infinity,
                              color: Colors.redAccent,
                              child: Text(
                                "<-",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: height/20
                                ),
                              ),
                            ),
                          )
                      )
                    ],
                  )
              ),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          child: MaterialButton(onPressed: () {  },
                            height: double.infinity,
                            color: Colors.white,
                            child: Text(
                              "4",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: height/20
                              ),
                            ),
                          ),
                        )
                    ),
                    Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          child: MaterialButton(onPressed: () {  },
                            height: double.infinity,
                            color: Colors.white,
                            child: Text(
                              "5",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: height/20
                              ),
                            ),
                          ),
                        )
                    ),
                    Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          child: MaterialButton(onPressed: () {  },
                            height: double.infinity,
                            color: Colors.white,
                            child: Text(
                              "6",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: height/20
                              ),
                            ),
                          ),
                        )
                    ),
                    Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          child: MaterialButton(onPressed: () {  },
                            height: double.infinity,
                            color: Colors.blueGrey,
                            child: Text(
                              "+",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: height/20
                              ),
                            ),
                          ),
                        )
                    )
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          child: MaterialButton(onPressed: () {  },
                            height: double.infinity,
                            color: Colors.white,
                            child: Text(
                              "7",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: height/20
                              ),
                            ),
                          ),
                        )
                    ),
                    Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          child: MaterialButton(onPressed: () {  },
                            height: double.infinity,
                            color: Colors.white,
                            child: Text(
                              "8",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: height/20
                              ),
                            ),
                          ),
                        )
                    ),
                    Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          child: MaterialButton(onPressed: () {  },
                            height: double.infinity,
                            color: Colors.white,
                            child: Text(
                              "9",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: height/20
                              ),
                            ),
                          ),
                        )
                    ),
                    Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          child: MaterialButton(onPressed: () {  },
                            height: double.infinity,
                            color: Colors.blueGrey,
                            child: Text(
                              "-",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: height/20
                              ),
                            ),
                          ),
                        )
                    )
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          child: MaterialButton(onPressed: () {  },
                            height: double.infinity,
                            color: Colors.blueGrey,
                            child: Text(
                              "/",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: height/20
                              ),
                            ),
                          ),
                        )
                    ),
                    Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          child: MaterialButton(onPressed: () {  },
                            height: double.infinity,
                            color: Colors.white,
                            child: Text(
                              "0",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: height/20
                              ),
                            ),
                          ),
                        )
                    ),
                    Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          child: MaterialButton(onPressed: () {  },
                            height: double.infinity,
                            color: Colors.blueGrey,
                            child: Text(
                              "*",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: height/20
                              ),
                            ),
                          ),
                        )
                    ),
                    Expanded(
                        child: Container(
                          margin: const EdgeInsets.all(2),
                          child: MaterialButton(onPressed: () {  },
                            height: double.infinity,
                            color: Colors.greenAccent,
                            child: Text(
                              "=",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontSize: height/20
                              ),
                            ),
                          ),
                        )
                    )
                  ],
                ),
              )
            ],
          ),
        )
    );
  }
}
